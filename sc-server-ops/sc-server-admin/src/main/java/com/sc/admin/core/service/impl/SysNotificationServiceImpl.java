/**
 * Created by wust on 2019-10-21 14:32:51
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;


import com.sc.admin.core.bo.NotificationBo;
import com.sc.admin.core.dao.SysNotificationMapper;
import com.sc.admin.core.service.SysNotificationService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:32:51
 * @description:
 *
 */
@Service("sysNotificationServiceImpl")
public class SysNotificationServiceImpl extends BaseServiceImpl<SysNotification> implements SysNotificationService {
    @Autowired
    private SysNotificationMapper sysNotificationMapper;

    @Autowired
    private NotificationBo notificationBo;



    @Override
    protected IBaseMapper getBaseMapper() {
        return sysNotificationMapper;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public WebResponseDto publish(Long id) {
        WebResponseDto responseDto = notificationBo.publish(id);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
