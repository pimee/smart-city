/**
 * Created by wust on 2019-10-21 14:29:51
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;


import com.sc.admin.core.dao.SysMyNoticeMapper;
import com.sc.admin.core.service.SysMyNoticeService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.mynotice.SysMyNotice;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:29:51
 * @description: 我收到的通知
 *
 */
@Service("sysMyNoticeServiceImpl")
public class SysMyNoticeServiceImpl extends BaseServiceImpl<SysMyNotice> implements SysMyNoticeService {

    @Autowired
    private SysMyNoticeMapper sysMyNoticeMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysMyNoticeMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Override
    public List<Map> getUnreadNoticeCount(String status, String receiver) {
        return sysMyNoticeMapper.getUnreadNoticeCount(status,receiver);
    }
}
