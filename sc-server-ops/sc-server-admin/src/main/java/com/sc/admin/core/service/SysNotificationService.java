package com.sc.admin.core.service;


import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import com.sc.common.service.BaseService;

public interface SysNotificationService  extends BaseService<SysNotification> {
    /**
     * 发布通知
     * @param id
     * @return
     */
    WebResponseDto publish(Long id);
}
