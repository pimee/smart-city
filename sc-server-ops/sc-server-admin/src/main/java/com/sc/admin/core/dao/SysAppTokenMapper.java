package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.apptoken.SysAppToken;

public interface SysAppTokenMapper extends IBaseMapper<SysAppToken> {
}