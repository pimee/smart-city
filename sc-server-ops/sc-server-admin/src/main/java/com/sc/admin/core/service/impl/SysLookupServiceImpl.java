package com.sc.admin.core.service.impl;


import com.sc.admin.core.service.SysLookupService;
import com.sc.admin.core.dao.SysLookupMapper;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wust on 2019/4/29.
 */
@Service("sysLookupServiceImpl")
public class SysLookupServiceImpl extends BaseServiceImpl<SysLookup> implements SysLookupService {

    @Autowired
    private SysLookupMapper sysLookupMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysLookupMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
