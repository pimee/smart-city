package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.userorganization.SysUserOrganization;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

public interface SysUserOrganizationMapper extends IBaseMapper<SysUserOrganization> {
    int deleteAll();

    List<Map> findBranchCompanyByAgentId(@Param("agentId") Long agentId);
    List<Map> findProjectByAgentId(@Param("agentId") Long agentId);

    List<Map> findBranchCompanyByParentCompanyId(@Param("parentCompanyId") Long parentCompanyId);
    List<Map> findProjectByParentCompanyId(@Param("parentCompanyId") Long parentCompanyId);

    List<Map> findBranchCompanyByBranchCompanyId(@Param("branchCompanyId") Long branchCompanyId);
    List<Map> findProjectByBranchCompanyId(@Param("branchCompanyId") Long branchCompanyId);

    List<Map> findBranchCompanyByProjectId(@Param("projectId") Long projectId);
    List<Map> findProjectByProjectId(@Param("projectId") Long projectId);

    List<Map> findBranchCompanyByUserId(@Param("userId") Long userId);
    List<Map> findProjectByUserId(@Param("userId") Long userId);
}