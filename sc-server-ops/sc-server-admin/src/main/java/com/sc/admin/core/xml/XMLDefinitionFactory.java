package com.sc.admin.core.xml;

public interface XMLDefinitionFactory {
    XMLAbstractResolver createXMLResolver();
}
