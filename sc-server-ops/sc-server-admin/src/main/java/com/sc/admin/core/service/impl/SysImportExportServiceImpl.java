package com.sc.admin.core.service.impl;


import com.sc.admin.core.dao.SysImportExportMapper;
import com.sc.admin.core.service.SysImportExportService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wust on 2019/5/20.
 */
@Service("sysImportExportServiceImpl")
public class SysImportExportServiceImpl extends BaseServiceImpl<SysImportExport> implements SysImportExportService {
    @Autowired
    private SysImportExportMapper sysImportExportMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysImportExportMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        return null;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        return null;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        return null;
    }
}
