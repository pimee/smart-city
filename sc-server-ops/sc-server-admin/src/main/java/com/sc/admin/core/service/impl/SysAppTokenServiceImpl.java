package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysAppTokenMapper;
import com.sc.admin.core.service.SysAppTokenService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("sysAppTokenServiceImpl")
public class SysAppTokenServiceImpl extends BaseServiceImpl<SysAppToken> implements SysAppTokenService {
    @Autowired
    private SysAppTokenMapper appTokenMapper;


    @Override
    protected IBaseMapper getBaseMapper() {
        return appTokenMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        SysAppToken entity = (SysAppToken)obj;
        appTokenMapper.insert(entity);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        SysAppToken entity = (SysAppToken)obj;
        appTokenMapper.updateByPrimaryKeySelective(entity);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        appTokenMapper.deleteByPrimaryKey(obj);
        return responseDto;
    }
}
