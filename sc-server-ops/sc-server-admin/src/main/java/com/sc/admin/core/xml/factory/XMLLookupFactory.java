package com.sc.admin.core.xml.factory;



import com.sc.admin.core.xml.XMLAbstractResolver;
import com.sc.admin.core.xml.XMLDefinitionFactory;
import com.sc.admin.core.xml.resolver.XMLLookupResolver;

/**
 *
 * Function:
 * Reason:
 * Date:2018/6/27
 * @author wust
 */
public class XMLLookupFactory implements XMLDefinitionFactory {

    @Override
    public XMLAbstractResolver createXMLResolver() {
        XMLAbstractResolver xmlAbstractResolver = new XMLLookupResolver();
        return xmlAbstractResolver;
    }
}
