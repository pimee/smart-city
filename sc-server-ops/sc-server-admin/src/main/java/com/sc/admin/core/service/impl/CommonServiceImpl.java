package com.sc.admin.core.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.*;
import com.sc.admin.core.service.CommonService;
import com.sc.admin.core.dao.*;
import com.sc.common.dao.CommonMapper;
import com.sc.common.entity.admin.organization.SysOrganization;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.entity.admin.role.resource.SysRoleResource;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.DataDictionaryEnum;
import com.sc.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date ：Created in 2019/9/7 9:57
 * @description：
 * @version:
 */
@Service("commonServiceImpl")
public class CommonServiceImpl implements CommonService {
    @Autowired
    private CommonMapper commonMapper;

    @Autowired
    private Environment environment;

    @Autowired
    private SysOrganizationMapper sysOrganizationMapper;

    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Autowired
    private SysProjectMapper sysProjectMapper;

    @Autowired
    private SysDepartmentMapper sysDepartmentMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserOrganizationMapper sysUserOrganizationMapper;

    @Autowired
    private SysRoleResourceMapper sysRoleResourceMapper;

    @Autowired
    private SysDataSourceMapper sysDataSourceMapper;

    @Autowired
    private SysAccountMapper sysAccountMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysCustomerMapper sysCustomerMapper;

    @Autowired
    private SysAppTokenMapper sysAppTokenMapper;




    @Override
    public List<Map<String, Object>> findBySql(Map<String, Object> parameters) throws DataAccessException {
        return commonMapper.findBySql(parameters);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void cleaningProjectAllData(Long projectId) {
        if("dev".equals(environment.getProperty("spring.profiles.active"))){ // 保证开发环境才能执行此操作
            SysProject project = sysProjectMapper.selectByPrimaryKey(projectId);
            if(project == null){
                return;
            }

            if(ApplicationEnum.DEFAULT_PROJECT_NAME.getStringValue().equals(project.getName())){
                throw new BusinessException("此项目属于系统默认项目，不允许删除");
            }

            SysOrganization organizationSearch = new SysOrganization();
            organizationSearch.setRelationId(projectId);
            SysOrganization organization = sysOrganizationMapper.selectOne(organizationSearch);
            if(organization != null){
                sysOrganizationMapper.deleteByPrimaryKey(organization.getId());

                // 删除组织对应的资源权限
                SysRoleResource roleResourceSearch = new SysRoleResource();
                roleResourceSearch.setOrganizationId(organization.getId());
                sysRoleResourceMapper.delete(roleResourceSearch);

                lookupDeleteOrganization(organization.getId());
            }

            // 删除项目
            sysProjectMapper.deleteByPrimaryKey(projectId);
        }
    }

    private void lookupDeleteOrganization(Long pid){
        SysOrganization organizationSearch = new SysOrganization();
        organizationSearch.setPid(pid);
        List<SysOrganization> organizations = sysOrganizationMapper.select(organizationSearch);
        if(CollectionUtil.isNotEmpty(organizations)){
            for (SysOrganization organization : organizations) {
                if(DataDictionaryEnum.ORGANIZATION_TYPE_PARENT_COMPANY.getStringValue().equals(organization.getType())){
                    sysCompanyMapper.deleteByPrimaryKey(organization.getRelationId());

                    sysOrganizationMapper.deleteByPrimaryKey(organization.getId());
                }else if(DataDictionaryEnum.ORGANIZATION_TYPE_BRANCH_COMPANY.getStringValue().equals(organization.getType())){
                    sysCompanyMapper.deleteByPrimaryKey(organization.getRelationId());

                    sysOrganizationMapper.deleteByPrimaryKey(organization.getId());
                }else if(DataDictionaryEnum.ORGANIZATION_TYPE_PROJECT.getStringValue().equals(organization.getType())){
                    sysProjectMapper.deleteByPrimaryKey(organization.getRelationId());

                    sysOrganizationMapper.deleteByPrimaryKey(organization.getId());
                }else if(DataDictionaryEnum.ORGANIZATION_TYPE_DEPARTMENT.getStringValue().equals(organization.getType())){
                    sysDepartmentMapper.deleteByPrimaryKey(organization.getRelationId());

                    sysOrganizationMapper.deleteByPrimaryKey(organization.getId());
                }else if(DataDictionaryEnum.ORGANIZATION_TYPE_ROLE.getStringValue().equals(organization.getType())){
                    sysRoleMapper.deleteByPrimaryKey(organization.getRelationId());

                    sysOrganizationMapper.deleteByPrimaryKey(organization.getId());

                    // 删除组织对应的资源权限
                    SysRoleResource roleResourceSearch = new SysRoleResource();
                    roleResourceSearch.setOrganizationId(organization.getId());
                    sysRoleResourceMapper.delete(roleResourceSearch);
                }
                lookupDeleteOrganization(organization.getId());
            }
        }
    }
}
