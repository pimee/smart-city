/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysOrganizationMapper;
import com.sc.admin.core.service.SysUserOrganizationService;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.organization.SysOrganization;
import com.sc.common.entity.admin.userorganization.SysUserOrganization;
import com.sc.common.enums.DataDictionaryEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 组织架构缓存
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysOrganization")
@Component
public class RedisCacheOrganizationBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysOrganizationMapper sysOrganizationMapper;

    // 组织结构有任何风吹草动都要重新初始化关系表
    @Autowired
    private SysUserOrganizationService sysUserOrganizationServiceImpl;

    @Override
    public void init() {
        Set<String> keys = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_ORGANIZATION_BY_RELATION_ID_AND_TYPE");
        if (keys != null && keys.size() > 0) {
            springRedisTools.deleteByKey(keys);
        }

        List<SysOrganization> organizationAll = sysOrganizationMapper.selectAll();
        if (CollectionUtil.isNotEmpty(organizationAll)) {
            for (SysOrganization organization : organizationAll) {
                Long id = organization.getId();
                String type = organization.getType();
                Long relationId = organization.getRelationId();

                cacheByTypeAndRelationId(type, relationId);
                cacheByPid(id);
            }
            sysUserOrganizationServiceImpl.init();
        }
    }

    @Override
    public void reset() {
        sysUserOrganizationServiceImpl.init();
    }

    @Override
    public void add(Object obj) {
        if (obj == null) {
            return;
        }

        SysOrganization entity = null;

        if (obj instanceof Long) {
            entity = sysOrganizationMapper.selectByPrimaryKey(obj);
        } else if (obj instanceof SysOrganization) {
            entity = (SysOrganization) obj;
        } else if (obj instanceof Map) {
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj), SysOrganization.class);
        }

        if (entity == null) {
            return;
        }

        Long id = entity.getId();
        String type = entity.getType();
        Long relationId = entity.getRelationId();

        cacheByTypeAndRelationId(type, relationId);
        cacheByPid(id);
        initByUserId(type, relationId);
    }

    @Override
    public void batchAdd(List<Object> list) {
        if (CollectionUtil.isNotEmpty(list)) {
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey) {
        SysOrganization organization = sysOrganizationMapper.selectByPrimaryKey(primaryKey);
        if (organization != null) {
            Long id = organization.getId();
            String type = organization.getType();
            Long relationId = organization.getRelationId();

            cacheByTypeAndRelationId(type, relationId);
            cacheByPid(id);
            initByUserId(type, relationId);
        }
    }

    @Override
    public void batchUpdate(List<Object> list) {
        if (CollectionUtil.isNotEmpty(list)) {
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey) {
        SysOrganization organization = sysOrganizationMapper.selectByPrimaryKey(primaryKey);
        if (organization != null) {
            String type = organization.getType();
            Long relationId = organization.getRelationId();

            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ORGANIZATION_BY_RELATION_ID_AND_TYPE.getStringValue(), relationId, type);
            if (springRedisTools.hasKey(key1)) {
                springRedisTools.deleteByKey(key1);
            }


            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ORGANIZATION_BY_PID.getStringValue(), organization.getId());
            if (springRedisTools.hasKey(key2)) {
                springRedisTools.deleteByKey(key2);
            }

            if (DataDictionaryEnum.ORGANIZATION_TYPE_USER.getStringValue().equals(type)) {
                SysUserOrganization userOrganizationSearch = new SysUserOrganization();
                userOrganizationSearch.setUserId(relationId);
                sysUserOrganizationServiceImpl.delete(userOrganizationSearch);
            }
        }
    }

    @Override
    public void batchDelete(List<Object> primaryKeys) {
        if (CollectionUtil.isNotEmpty(primaryKeys)) {
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }


    /**
     * 根据type和relationId分组组织结构数据
     * @param type
     * @param relationId
     */
    private void cacheByTypeAndRelationId(String type, Long relationId) {
        SysOrganization search = new SysOrganization();
        search.setRelationId(relationId);
        search.setType(type);
        List<SysOrganization> organizationList = sysOrganizationMapper.select(search);
        if (CollectionUtil.isNotEmpty(organizationList)) {
            Map<Long, SysOrganization> mapValue1 = new HashMap(organizationList.size());
            for (SysOrganization sysOrganization : organizationList) {
                mapValue1.put(sysOrganization.getId(), sysOrganization);
            }

            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ORGANIZATION_BY_RELATION_ID_AND_TYPE.getStringValue(), relationId, type);
            if (springRedisTools.hasKey(key1)) {
                springRedisTools.deleteByKey(key1);
            }
            springRedisTools.addMap(key1, mapValue1);
        }
    }

    /**
     * 根据pid分组组织结构数据
     * @param id
     */
    private void cacheByPid(Long id) {
        SysOrganization organizationSearch = new SysOrganization();
        organizationSearch.setPid(id);
        List<SysOrganization> children = sysOrganizationMapper.select(organizationSearch);
        if (CollectionUtil.isNotEmpty(children)) {
            Map mapValue2 = new HashMap();
            for (SysOrganization child : children) {
                mapValue2.put(child.getId(), child);
            }

            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ORGANIZATION_BY_PID.getStringValue(), id);
            if (springRedisTools.hasKey(key2)) {
                springRedisTools.deleteByKey(key2);
            }
            springRedisTools.addMap(key2, mapValue2);
        }
    }

    /**
     * 员工在组织中有变动，则重新初始化该员工对应的UserOrganization
     * @param type
     * @param relationId
     */
    private void initByUserId(String type, Long relationId) {
        if(DataDictionaryEnum.ORGANIZATION_TYPE_USER.getStringValue().equals(type)){
            sysUserOrganizationServiceImpl.initByUser(relationId);
        }
    }
}
