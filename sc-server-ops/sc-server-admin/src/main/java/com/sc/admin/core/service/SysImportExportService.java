package com.sc.admin.core.service;


import com.sc.common.entity.admin.importexport.SysImportExport;
import com.sc.common.service.BaseService;


/**
 * Created by wust on 2019/5/20.
 */
public interface SysImportExportService extends BaseService<SysImportExport> {
}
