/**
 * Created by wust on 2020-04-18 17:08:15
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.gateway.core;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.common.util.MyStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import java.util.*;

/**
 * @author: wust
 * @date: Created in 2020-04-18 17:08:15
 * @description: 用于加载注册中心的服务
 *
 */
@Component
public class SwaggerProvider implements SwaggerResourcesProvider {
    @Value("${swagger2url}")
    private String swagger2url;

    private final RouteLocator routeLocator;

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    public SwaggerProvider(RouteLocator routeLocator) {
        this.routeLocator = routeLocator;
    }

    private static final Map<String,String> serverNameMap = new HashMap<>();
    static {
        serverNameMap.put("admin-server","基础管理服务");
        serverNameMap.put("demo1-server","demo1服务");
        serverNameMap.put("work-order-server","工单中心服务");
        serverNameMap.put("data-centers-server","数据中心服务");
        serverNameMap.put("businesslog-server","业务日志服务");
        serverNameMap.put("autotask-server","任务调度服务");
    }

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        List<String> routeHosts = new ArrayList<>();
        // 获取所有可用的host：serviceId
        routeLocator.getRoutes().filter(route -> route.getUri().getHost() != null)
                .filter(route -> !applicationName.equals(route.getUri().getHost()))
                .subscribe(route -> routeHosts.add(route.getUri().getHost()));
        // 记录已经添加过的server，存在同一个应用注册了多个服务在注册中心上
        Set<String> dealed = new HashSet<>();
        routeHosts.forEach(instance -> {
            if(!applicationName.equalsIgnoreCase(instance)){
                if(MyStringUtils.isNotBlank(MyStringUtils.null2String(instance))){
                    String url = "/" + instance.toLowerCase() + swagger2url;
                    if (!dealed.contains(url)) {
                        dealed.add(url);
                        SwaggerResource swaggerResource = new SwaggerResource();
                        swaggerResource.setUrl(url);
                        swaggerResource.setName(serverNameMap.get(instance.toLowerCase()));
                        resources.add(swaggerResource);
                    }
                }
            }
        });

        List<SwaggerResource> resourcesNotEmpty = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(resources)){
            for (SwaggerResource resource : resources) {
                if(resource == null || MyStringUtils.isBlank(MyStringUtils.null2String(resource.getName()))){
                    continue;
                }
                resourcesNotEmpty.add(resource);
            }
        }
        return resourcesNotEmpty;
    }
}
