package com.sc.workorder.entity;


/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
public class WoWorkOrderList extends WoWorkOrder {
    private static final long serialVersionUID = -3655908779375087569L;

    private String directorLabel;
    private String workOrderTypeLabel;
    private String stateLabel;
    private String applicantLabel;
    private String priorityLevelLabel;

    public String getDirectorLabel() {
        return directorLabel;
    }

    public void setDirectorLabel(String directorLabel) {
        this.directorLabel = directorLabel;
    }

    public String getWorkOrderTypeLabel() {
        return workOrderTypeLabel;
    }

    public void setWorkOrderTypeLabel(String workOrderTypeLabel) {
        this.workOrderTypeLabel = workOrderTypeLabel;
    }

    public String getStateLabel() {
        return stateLabel;
    }

    public void setStateLabel(String stateLabel) {
        this.stateLabel = stateLabel;
    }

    public String getApplicantLabel() {
        return applicantLabel;
    }

    public void setApplicantLabel(String applicantLabel) {
        this.applicantLabel = applicantLabel;
    }

    public String getPriorityLevelLabel() {
        return priorityLevelLabel;
    }

    public void setPriorityLevelLabel(String priorityLevelLabel) {
        this.priorityLevelLabel = priorityLevelLabel;
    }
}