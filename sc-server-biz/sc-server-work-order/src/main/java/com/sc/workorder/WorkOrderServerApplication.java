package com.sc.workorder; /**
 * Created by wust on 2020-03-31 13:13:30
 * Copyright © 2020 wust. All rights reserved.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author: wust
 * @date: Created in 2020-03-31 13:13:30
 * @description:
 */
@EnableSwagger2
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.workorder.core.dao"})
@ComponentScan(basePackages = {"com.sc"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {}))
@SpringBootApplication
public class WorkOrderServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(WorkOrderServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(WorkOrderServerApplication.class, args);
    }
}
