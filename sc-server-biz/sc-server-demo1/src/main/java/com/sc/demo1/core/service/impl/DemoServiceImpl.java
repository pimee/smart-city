package com.sc.demo1.core.service.impl;


import com.sc.common.mapper.IBaseMapper;
import com.sc.demo1.entity.demo.Demo;
import com.sc.common.service.BaseServiceImpl;
import com.sc.demo1.core.dao.DemoMapper;
import com.sc.demo1.core.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：wust
 * @date ：Created in 2019/9/9 11:24
 * @description：
 * @version:
 */
@Service("demoServiceImpl")
public class DemoServiceImpl extends BaseServiceImpl<Demo> implements DemoService {
    @Autowired
    private DemoMapper demoMapper;


    @Override
    protected IBaseMapper getBaseMapper() {
        return demoMapper;
    }
}
