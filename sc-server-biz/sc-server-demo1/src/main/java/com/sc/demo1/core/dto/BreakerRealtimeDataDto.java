/**
 * Created by wust on 2019-10-16 14:57:00
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo1.core.dto;


import com.sc.common.util.MyStringUtils;

/**
 * @author: wust
 * @date: Created in 2019-10-16 14:57:00
 * @description: 断路器实时数据 数据传输对象
 *
 */
public class BreakerRealtimeDataDto implements java.io.Serializable{
    private static final long serialVersionUID = 3262872203531915695L;


    private String gatewayMac;

    private String breakerSn;


    /**
     * 电压
     */
    private String voltage;


    /**
     * 电流
     */
    private String electricalCurrent;

    /**
     * 漏电电流
     */
    private String leakageCurrent;

    /**
     * 有功功率
     */
    private String power;

    /**
     * 无功功率
     */
    private String reactivePower;

    /**
     * 功率因素
     */
    private String powerFactor;

    /**
     * 温度
     */
    private String temperature;


    /**
     * 接收数据时间
     */
    private String dateTimeByReceiving ;

    public String getGatewayMac() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(gatewayMac))){
            return "/";
        }
        return gatewayMac;
    }

    public void setGatewayMac(String gatewayMac) {
        this.gatewayMac = gatewayMac;
    }

    public String getBreakerSn() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(breakerSn))){
            return "/";
        }
        return breakerSn;
    }

    public void setBreakerSn(String breakerSn) {
        this.breakerSn = breakerSn;
    }

    public String getVoltage() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(voltage))){
            return "/";
        }
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getElectricalCurrent() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrent))){
            return "/";
        }
        return electricalCurrent;
    }

    public void setElectricalCurrent(String electricalCurrent) {
        this.electricalCurrent = electricalCurrent;
    }

    public String getLeakageCurrent() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(leakageCurrent))){
            return "/";
        }
        return leakageCurrent;
    }

    public void setLeakageCurrent(String leakageCurrent) {
        this.leakageCurrent = leakageCurrent;
    }

    public String getPower() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(power))){
            return "/";
        }
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getReactivePower() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(reactivePower))){
            return "/";
        }
        return reactivePower;
    }

    public void setReactivePower(String reactivePower) {
        this.reactivePower = reactivePower;
    }

    public String getPowerFactor() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerFactor))){
            return "/";
        }
        return powerFactor;
    }

    public void setPowerFactor(String powerFactor) {
        this.powerFactor = powerFactor;
    }

    public String getTemperature() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(temperature))){
            return "/";
        }
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDateTimeByReceiving() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(dateTimeByReceiving))){
            return "/";
        }
        return dateTimeByReceiving;
    }

    public void setDateTimeByReceiving(String dateTimeByReceiving) {
        this.dateTimeByReceiving = dateTimeByReceiving;
    }
}
