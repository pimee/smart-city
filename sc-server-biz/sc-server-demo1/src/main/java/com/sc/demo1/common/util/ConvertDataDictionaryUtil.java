package com.sc.demo1.common.util;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author ：wust
 * @date ：Created in 2019/9/25 15:01
 * @description：数据字典转换工具，主要用于旧版本的数据转换到新版本的数据字典
 * @version:
 */
public class ConvertDataDictionaryUtil {
    static Logger logger = LogManager.getLogger(ConvertDataDictionaryUtil.class);

    private ConvertDataDictionaryUtil(){}

    public static String convertBreakerModel(String model){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1018");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(model.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    /**
     * 转换断路器状态
     * @param status
     * @return
     */
    public static String convertBreakerStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1016");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertContactorStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1014");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }


    public static String convertContactorKeyStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1041");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertContactorKeyType(String type){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1022");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(type.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertPanelStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1014");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertSensorStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1014");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertOnOff(String value){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1023");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(value.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertSensorKeyDataKey(String dataKey){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1025");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(dataKey.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }

    public static String convertSuccessFlag(String value){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"A1007");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(value.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }


    /**
     * 转换传感器型号
     * @param model
     * @return
     */
    public static String convertSensorModel(String model){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1024");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(model.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }


    /**
     * 转换断路器操作状态
     * @param status
     * @return
     */
    public static String convertBreakerOperationStatus(String status){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1039");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(status.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }
}
