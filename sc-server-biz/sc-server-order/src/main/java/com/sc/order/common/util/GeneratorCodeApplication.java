package com.sc.order.common.util;


import com.sc.generatorcode.invoker.SingleInvoker;
import com.sc.generatorcode.invoker.base.Invoker;

/**
 * 生成代码入口
 */
public class GeneratorCodeApplication {

    public static void main(String[] args) {
        Invoker invoker = new SingleInvoker.Builder()
                .setTableName("t_order_detail")
                .setClassName("OrderDetail")
                .build();
        invoker.execute();
    }
}
