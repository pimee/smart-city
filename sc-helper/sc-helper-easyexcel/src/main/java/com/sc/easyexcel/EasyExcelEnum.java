package com.sc.easyexcel;/**
 * Created by wust on 2017/5/17.
 */

/**
 *
 * Function:
 * Reason:
 * Date:2017/5/17
 * @author wust
 */
public enum EasyExcelEnum {
    String,
    Long,
    Integer,
    Double,
    Float,
    Date,
    Map,
    LookupItem,
    excel,
    sheet,
    list,
    sql,
    field,
    label,
    column,
    type,
    format,
    id,
    index,
    property,
    pattern,
    required,
    startRow,
    rootCode,
    regex,
    regexErrMsg,
    complexReport,
    row,
    cell,
    rowspan,
    colspan,
    scale,
    roundingMode
}
