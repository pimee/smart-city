package com.sc.common.enums;

public enum ReturnCode {
    SUCCESS("OK","成功"),

    /* 1000～1999：参数错误 */
    INVALID_PARAMETERS_NULL("1000","参数空"),
    INVALID_PARAMETERS_ILLEGAL("1000","非法的参数格式");

    /* 2000～2999：用户错误 */

    /* 3000～3999：接口错误 */

    /* 4000～4999：授权错误 */



    private String code;
    private String message;

    ReturnCode(String code,String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
