package com.sc.common.entity.admin.account;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
@EnableLocalCaching
@EnableDistributedCaching
public class SysAccount extends BaseEntity {
    private static final long serialVersionUID = 5322750702842174767L;
    private String accountCode;
    private String accountName;
    private String password;
    private String type;
    private String email;
    private String contactNumber;
    private String source;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}