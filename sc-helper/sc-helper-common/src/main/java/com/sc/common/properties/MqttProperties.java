package com.sc.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author ：wust
 * @date ：Created in 2019/9/19 16:12
 * @description：
 * @version:
 */
@ConfigurationProperties(prefix="spring.mqtt")
public class MqttProperties {
    private MqttInboundProperties inbound;

    private MqttOutboundProperties outbound;

    public MqttInboundProperties getInbound() {
        return inbound;
    }

    public void setInbound(MqttInboundProperties inbound) {
        this.inbound = inbound;
    }

    public MqttOutboundProperties getOutbound() {
        return outbound;
    }

    public void setOutbound(MqttOutboundProperties outbound) {
        this.outbound = outbound;
    }
}
