package com.sc.feign.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@EnableFeignClients({"com.sc"})
@Configuration
public class FeignAutoConfiguration {
}
