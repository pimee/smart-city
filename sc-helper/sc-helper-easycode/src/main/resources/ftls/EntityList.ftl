package ${EntityPackageName};

import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
  @ApiModel(description = "列表对象-${ClassName}List")
  @NoArgsConstructor
  @ToString
  @Data
public class ${ClassName}List extends ${ClassName} {

}