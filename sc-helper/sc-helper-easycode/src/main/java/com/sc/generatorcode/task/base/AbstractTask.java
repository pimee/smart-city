package com.sc.generatorcode.task.base;

import com.sc.generatorcode.entity.ColumnInfo;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public abstract class AbstractTask implements Serializable {
    protected String tableName;
    protected String className;
    protected String parentTableName;
    protected String parentClassName;
    protected String foreignKey;
    protected String relationalTableName;
    protected String parentForeignKey;
    protected List<ColumnInfo> columnInfos;
    protected List<ColumnInfo> parentTableInfos;


    public AbstractTask(String className) {
        this.className = className;
    }


    public AbstractTask(String className, List<ColumnInfo> columnInfos) {
        this.className = className;
        this.columnInfos = columnInfos;
    }

    public AbstractTask(String className, String tableName,List<ColumnInfo> columnInfos) {
        this.className = className;
        this.tableName = tableName;
        this.columnInfos = columnInfos;
    }

    /**
     * Entity
     *
     * @param className
     * @param parentClassName
     * @param foreignKey
     * @param columnInfos
     */
    public AbstractTask(String className, String parentClassName, String foreignKey, String parentForeignKey, List<ColumnInfo> columnInfos) {
        this.className = className;
        this.parentClassName = parentClassName;
        this.foreignKey = foreignKey;
        this.parentForeignKey = parentForeignKey;
        this.columnInfos = columnInfos;
    }


    /**
     * Mapper
     *
     * @param tableName
     * @param className
     * @param parentTableName
     * @param parentClassName
     * @param foreignKey
     * @param parentForeignKey
     * @param columnInfos
     * @param parentTableInfos
     */
    public AbstractTask(String tableName, String className, String parentTableName, String parentClassName, String foreignKey, String parentForeignKey, String relationalTableName, List<ColumnInfo> columnInfos, List<ColumnInfo> parentTableInfos) {
        this.tableName = tableName;
        this.className = className;
        this.parentTableName = parentTableName;
        this.parentClassName = parentClassName;
        this.foreignKey = foreignKey;
        this.parentForeignKey = parentForeignKey;
        this.relationalTableName = relationalTableName;
        this.columnInfos = columnInfos;
        this.parentTableInfos = parentTableInfos;
    }

    public abstract void run() throws IOException, TemplateException;


    public String[] getName(){
        int j = 0;
        for (int i = 0; i < className.length(); i++) {
            if (i != 0 && Character.isUpperCase(className.charAt(i))) {
                j = i;
                break;
            }
        }

        String postfixName = className.substring(j);

        String name = "";
        for(int i = 0; i < postfixName.length(); i ++){
            if(Character.isUpperCase(postfixName.charAt(i))){
                if(i == 0){
                    name += Character.toLowerCase(postfixName.charAt(i));
                }else{
                    name += "-" + Character.toLowerCase(postfixName.charAt(i));
                }
            }else{
                name += postfixName.charAt(i);
            }
        }
        return new String[]{postfixName,name};
    }

}
